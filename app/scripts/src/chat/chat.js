$(document).ready(function () {

    $(document).ajaxStop(function () {
        $.unblockUI();
    });

    $(document).ajaxStart(function () {
        $.blockUI();
    });

    $("#action").change(function () {
        var val = $(this).val();
        doAction(val);
    });

    createChosen();
    $('#to').bind("change", function () {
        $('#whatsappform').validate().element($(this));
    });

    $('#from').bind("change", function () {
        doAction('getContacts');
    });

    function doAction(type)
    {
        switch (type) {
            case 'sendMessage':
                $('#to').prop('multiple', false);
                $("#to").prop('disabled', false);
                createChosen();
                $("#to").closest('div.form-group').show();
                $('#emojiTab').show();
                $("#faketextbox").prop('disabled', false);
                $("#faketextbox").closest('div.form-group').show();
                $("#image").prop('disabled', false);
                $("#image").closest('div.form-group').show();
                $("#audio").prop('disabled', false);
                $("#audio").closest('div.form-group').show();
                $("#video").prop('disabled', false);
                $("#video").closest('div.form-group').show();
                $("#locationname").prop('disabled', false);
                $("#userlat").prop('disabled', false);
                $("#userlong").prop('disabled', false);
                $("#locationname").closest('div.form-group').show();
                $("#pickLocation").show();
                $("#status").prop('disabled', true);
                $("#status").closest('div.form-group').hide();
                break;

            case 'updateStatus':
                $("#to").prop('disabled', true);
                $("#to").closest('div.form-group').hide();
                $('#emojiTab').hide();
                $("#faketextbox").prop('disabled', true);
                $("#faketextbox").closest('div.form-group').hide();
                $("#image").prop('disabled', true);
                $("#image").closest('div.form-group').hide();
                $("#audio").prop('disabled', true);
                $("#audio").closest('div.form-group').hide();
                $("#video").prop('disabled', true);
                $("#video").closest('div.form-group').hide();
                $("#locationname").prop('disabled', true);
                $("#userlat").prop('disabled', true);
                $("#userlong").prop('disabled', true);
                $("#locationname").closest('div.form-group').hide();
                $("#pickLocation").hide();
                $("#status").prop('disabled', false);
                $("#status").closest('div.form-group').show();
                break;

            case 'sendBroadcast':
                $('#to').prop('multiple', true);
                $("#to").prop('disabled', false);
                createChosen();
                $("#to").closest('div.form-group').show();
                $('#emojiTab').show();
                $("#faketextbox").prop('disabled', false);
                $("#faketextbox").closest('div.form-group').show();
                $("#image").prop('disabled', false);
                $("#image").closest('div.form-group').show();
                $("#audio").prop('disabled', false);
                $("#audio").closest('div.form-group').show();
                $("#video").prop('disabled', false);
                $("#video").closest('div.form-group').show();
                $("#locationname").prop('disabled', false);
                $("#userlat").prop('disabled', false);
                $("#userlong").prop('disabled', false);
                $("#locationname").closest('div.form-group').show();
                $("#pickLocation").show();
                $("#status").prop('disabled', true);
                $("#status").closest('div.form-group').hide();
                break;

            case 'getContacts':
                var fromuser = $("#from").val();
                $.ajax({
                    type: "POST",
                    url: "",
                    cache: false,
                    data: {action: "getContacts", from: fromuser},
                    dataType: "json",
                    timeout: 1500000,
                    success: onSuccess,
                    error: onError,
                    complete: function (jqXHR, textStatus) {
                        $.unblockUI();
                    }
                });

                return false;

            default:
                alert('Error. An action should have been specified.');

                return false;
        }
    }

    function createChosen()
    {
        $('#to_chzn').remove();
        $('#to').removeClass('chzn-done');
        $("#to").chosen({
            create_option: true,
            persistent_create_option: true,
            no_results_text: "Can't Find:",
            create_option_text: 'Click to Add '
        });
        $('#to_chzn').css('width', '220px');
    }
    ;

    $("img").on('click', function () {
        var txtToAdd = this.outerHTML;
        $("#faketextbox").append(txtToAdd);
    });

    $('#mapContainer').hide();
    $('#pickLocation').click(function () {
        if ($('#mapContainer').is(':visible')) {
            $('#mapContainer').hide();

            return false;
        } else {
            $('#mapContainer').show();
            createMap();

            return false;
        }
    });

    function createMap()
    {
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            center: new google.maps.LatLng(53.4, -7.778),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 6,
            panControl: false,
            streetViewControl: true,
            streetViewOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_CENTER
            }
        });

        var input = (document.getElementById('target'));
        var searchBox = new google.maps.places.SearchBox(input);
        var marker;

        google.maps.event.addListener(searchBox, 'places_changed', function () {
            var places = searchBox.getPlaces();
            placeMarker(places[0].geometry.location);
            map.setZoom(16);
            map.setCenter(marker.getPosition());
        });

        function placeMarker(location)
        {
            if (marker) {
                marker.setPosition(location);
            } else {
                marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }
            updateLocation(location);
        }

        function updateLocation(event)
        {
            if ($('#target').val() !== '') {
                $('#locationname').val($('#target').val());
            }
            $('#userlat').val(event.lat().toFixed(5));
            $('#userlong').val(event.lng().toFixed(6));
        }

        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng": event.latLng
            }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    //                                        var lat = results[0].geometry.location.lat(),
                    //                                                lng = results[0].geometry.location.lng();
                    $("#locationname").val(results[0].formatted_address);
                }
            });
        });
    }

    //if form is submitted
    $('#whatsappform').submit(function (e) {
        if ($('#whatsappform').validate().form() === false) {
            return false;
        }

        //Store original html to replace back in box later.
        var original = $('#faketextbox').html();
        //Scan html code for emojis and replace with text and special marker.
        $('#faketextbox img').each(function (index) {
            var emojiUnicode = this.outerHTML.match(/emoji-(.*?)"/)[1];
            $(this).replaceWith('##' + emojiUnicode + '##');
        });
        //Replace all BR's with line breaks.
        var message = $.trim($('#faketextbox').html().replace(/<br\s?\/?>/g, "\n"));
        //Copy the corrected message text to our hidden input field to be serialised.
        $('#message').val($('#faketextbox').html(message).text());
        //Replace the corrected text with the original html so it shows properly on a browser.
        $('#faketextbox').html(original);
        //Continue with the form.
        var formData = $("#whatsappform").serialize();

        $.ajax({
            type: "POST",
            url: "",
            cache: false,
            data: formData,
            dataType: "json",
            timeout: 4500000,
            success: onSuccess,
            error: onError,
            //beforeSend: function(jqXHR, settings) {
            //},
            complete: function () {
                $.unblockUI();
            }
        });

        return false;
    });

    $("#whatsappform").validate({
        ignore: ":hidden:not(select)",
        rules: {
            from: {
                required: true
            },
            to: {
                required: true
            },
            status: {
                minlength: 4,
                required: true
            },
            password: {
                minlength: 2,
                required: true
            }
        },
        errorPlacement: function (error, element) {
            return true;
        },
        highlight: function (label) {
            $(label).closest('.form-group').addClass('danger');
            if (label.id === 'to') {
                $('div#to_chzn a').addClass('danger');
                $('div#to_chzn a').removeClass('success');
            }
        },
        success: function (label) {
            $("#" + label[0].htmlFor).closest('.form-group').addClass('success');
            $("#" + label[0].htmlFor).closest('.form-group').removeClass('danger');
            if (label[0].htmlFor === 'to') {
                $('div#to_chzn a').addClass('success');
                $('div#to_chzn a').removeClass('danger');
            }
        }

    });

    function onSuccess(data, textStatus, jqXHR)
    {
        switch (data.success) {

            case false:
                newAlert('danger', data.errormsg);
                break;

            case true:
                if (data.type === 'contacts') {
                    $("select#to").html('<option></option>');
                    $.each(data.data, function (i, item) {
                        $("select#to").append("<option value='" + data.data[i].id + "'>" + data.data[i].name + "</option>");
                    });
                    $("#to").trigger("liszt:updated");
                } else {
                    newAlert('success', data.data);
                }
                if (data.messages !== null) {
                    $.each(data.messages, function (i, item) {
                        $("#inboundMessage").append("<div class='alert alert-block alert-info'>" + data.messages[i].time + ":<br /> " + data.messages[i].name + " (+" + data.messages[i].from + ")<br />" + data.messages[i].data + "</div>");
                    });
                }
                break;

            default:
                newAlert('success', 'The ajax call was successful but there was an error with the data returned: HTTP Status:' + jqXHR.status + "StatusText" + jqXHR.statusText);
                break;
        }
    }

    function onError(request, type, errorThrown)
    {
        var message = "There was an error with the AJAX request.\n";
        switch (type) {
            case 'timeout':
                message += "The request timed out.";
                break;
            case 'notmodified':
                message += "The request was not modified but was not retrieved from the cache.";
                break;
            case 'parsererror':
                message += "XML/Json format is bad.";
                break;
            default:
                message += "HTTP Error (" + request.status + " " + request.statusText + ")\n\n." + request.responseText + errorThrown;
        }
        $(".alert-danger").remove();
        newAlert('danger', message);
    }

    function newAlert(type, message)
    {
        var title = (type === 'danger') ? 'error' : type; // Fix for print "Error" instead "Danger"
        $("#results").append("<div class='alert alert-block alert-" + type + "' ><strong>" + title.charAt(0).toUpperCase() + title.substr(1) + ": </strong>" + message + "</div>");
        $(".alert-success").delay(5000).fadeOut("slow", function () {
            $(".alert-danger").remove();
            $(this).remove();
        });
    }

    $('#logout').bind("click", function () {
        $.ajax({
            type: "POST",
            url: "",
            data: {action: "logout"},
            success: function (result) {
                window.location.href = "/client/Chat-API-3.2.0.1/examples/whatsapp.php";
            },
            async: false
        });
    });
});